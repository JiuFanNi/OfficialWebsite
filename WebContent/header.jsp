<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	 <link rel="stylesheet" href="css/nstyle.css"> 
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="top_header">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                   <img src="img/logoH.png" alt="" style="width:46px;height: 40px;margin-top: 5px;margin-right: 10px">
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav main">
                        <li><a href="index.jsp">首页</a></li>
                        <li><a href="information.jsp">公司简介</a></li>
                        <li><a href="index.jsp#contents">主营业务</a></li>
                        <li><a href="products.jsp">产品</a></li>
                        <li><a href="contact.jsp">联系我们</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                        <s:if test=" #session.admName == null || #session.admName.isEmpty()" >
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">个人中心<span class="caret"></span></a>
                            
                            <ul class="dropdown-menu">
                                <li><a href="login.html">登录</a></li>
                                <li><a href="register.html">注册</a></li>
                            </ul>
                            </s:if>
                            <s:else>
    	  						${admName}
    	  					</s:else>   
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
</body>
</html>