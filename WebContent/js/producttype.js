var url;



function typetid(val,row){
	return row.tid;
}


function typepid(val,row){
	return row.pid;
}


function openproducttypeAddDialog(){
	$("#dlg").dialog("open").dialog("setTitle","添加商品类信息");
	$("#Tname").combotree({
		url:'productTypeAction_parentsType',
		panelHeight:'auto',
		editable:false
	});
	url="productTypeAction_insertType";
}


function openproducttypeModifyDialog(){
	var selectedRows=$("#dg").datagrid('getSelections');
	if(selectedRows.length!=1){
		$.messager.alert("系统提示","请选择一条要编辑的数据！");
		return;
	}
	var row=selectedRows[0];
	$("#win").dialog("open").dialog("setTitle","编辑商品类信息");
	$("#testid").val(row.tid);
	$("#testpid").val(row.pid);
	$("#testtname").val(row.tname);
	url="productTypeAction_updateType?s_type.tid="+row.tid;
}



function saveproducttype(){
	$("#fm").form("submit",{
		url:url,
		success:function(result){
			var result=eval('('+result+')');
			if(result.success){
				$.messager.alert("系统提示","保存成功");
				resetValue();
				$("#dlg").dialog("close");
				$("#dg").datagrid("reload");
			}else{
				$.messager.alert("系统提示","保存失败");
				return;
			}
		}
	});
}


function saveproducttype2(){
	$("#fm2").form("submit",{
		url:url,
		success:function(result){
			var result=eval('('+result+')');
			if(result.success){
				$.messager.alert("系统提示","保存成功");
				resetValue();
				$("#win").dialog("close");
				$("#dg").datagrid("reload");
			}else{
				$.messager.alert("系统提示","保存失败");
				return;
			}
		}
	});
}
function deleteproducttype(){
	var selectedRows=$("#dg").datagrid('getSelections');
	if(selectedRows.length==0){
		$.messager.alert("系统提示","请选择要删除的数据！");
		return;
	}
	var strIds=[];
	for(var i=0;i<selectedRows.length;i++){
		strIds.push(selectedRows[i].tid);
	}
	var ids=strIds.join(",");
	$.messager.confirm("系统提示","您确认要删除这<font color=red>"+selectedRows.length+"</font>条数据吗？",function(r){
		if(r){
			$.post("productTypeAction_deleteType",{ids:ids},function(result){
				if(result.success){
					$.messager.alert("系统提示","数据已成功删除！");
					$("#dg").datagrid("reload");
				}else{
					$.messager.alert("系统提示","数据删除失败！");
				}
			},"json");
		}
	});
	
}




function closeproducttypeDialog(){
	$("#dlg").dialog("close");
	resetValue();
}



function resetValue(){
	$("#tid").val("");
	$("#tname").val("");
	$("#Tname").combotree("setValue","");
}