var url;
	
	
	function typeid(val,row){
		return row.type.tid;
	}
	
	
	function typename(val,row){
		return row.type.tname;
	}

	function openProductAddDialog(){
		$("#dlg").dialog("open").dialog("setTitle","添加商品信息");
		$("#Tname").combotree({
			url:'productTypeAction_parentsType',
			panelHeight:'auto',
			editable:false
		});
		url="productAction_savaProduct";
	}
	function openProductModifyDialog(){
		var selectedRows=$("#dg").datagrid('getSelections');
		if(selectedRows.length!=1){
			$.messager.alert("系统提示","请选择一条要编辑的数据！");
			return;
		}
		
		var row=selectedRows[0];
		$("#dlg").dialog("open").dialog("setTitle","编辑商品信息");
		$("#name").val(row.name);
		$("#price").val(row.price);
		$("#path").val(row.path);
		$("#productpath").val(row.productpath);
		$("#description").val(row.description);
		$("#Tname").combotree("setValue",row.type.tid);
		url="productAction_savaProduct?product.id="+row.id;
	}
	function saveProduct(){
		$("#fm").form("submit",{
			url:url,
			onSubmit:function(){
				return $(this).form("validate");
			},
			success:function(result){
				var result=eval('('+result+')');
				if(result.success){
					$.messager.alert("系统提示","保存成功");
					resetValue();
					$("#dlg").dialog("close");
					$("#dg").datagrid("reload");
				}else{
					$.messager.alert("系统提示","保存失败");
					return;
				}
			}
		});
		
	}
	
	
	function deleteProduct(){
		var selectedRows=$("#dg").datagrid('getSelections');
		if(selectedRows.length==0){
			$.messager.alert("系统提示","请选择要删除的数据！");
			return;
		}
		var strIds=[];
		for(var i=0;i<selectedRows.length;i++){
			strIds.push(selectedRows[i].id);
		}
		var ids=strIds.join(",");
		$.messager.confirm("系统提示","您确认要删除这<font color=red>"+selectedRows.length+"</font>条数据吗？",function(r){
			if(r){
				$.post("productAction_deletProduct",{ids:ids},function(result){
					if(result.success){
						$.messager.alert("系统提示","数据已成功删除！");
						$("#dg").datagrid("reload");
					}else{
						$.messager.alert("系统提示","数据删除失败！");
					}
				},"json");
			}
		});
	}
	
	
	function resetValue(){
		$("#name").val("");
		$("#price").val("");
		$("#pt").val("");
		$("#Tname").combotree("setValue","");
		$("#description").val("");
		$("#path").val("");
		$("#ppt").val("");
		$("#productpath").val("");
	}
	
	function closeProductDialog(){
		$("#dlg").dialog("close");
		resetValue();
	}
