<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="x-ua-compatible" content="ie=7" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>北京华诚兴业软件开发有限责任公司</title>
    <link rel="icon" type="image/png" href="img/logoH.png" sizes="96*96">
    <link rel="stylesheet" href="css/nstyle.css">
    <link rel="stylesheet" href="css/swiper.min.css">
	<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
	 <style>
        .main li:nth-child(1){
            background: #fff!important;
        }
        .main li:nth-child(1) a{
            color: #000000!important;
        }
    </style>
</head>
<body>
<%@include file="header.jsp" %>
<!--banner-->
<div class="swiper-container">
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <img src="img/bg_member.jpg" alt="">
           <!--  <div>深度学习</div> -->
        </div>
        <div class="swiper-slide">
            <img src="img/bg_shoulei.jpg" alt="">
           <!--  <div>虚拟现实</div> -->
        </div>
        <div class="swiper-slide">
            <img src="img/bg_xav.jpg" alt="">
           <!--  <div>软件开发</div> -->
        </div>
        <div class="swiper-slide">
            <img src="img/bg_xkn.jpg" alt="">
           <!--  <div>人工智能</div> -->
        </div>
        <div class="swiper-slide">
            <img src="img/bg_xnet.jpg" alt="">
           <!--  <div>深度学习</div> -->
        </div>
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
    <!-- Add Arrows -->
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
</div>
<!--content-->
<div class="contents" id="contents">

    <div class="main_content_box" style="border: none">
        <div class="item">
            <h2>主营业务</h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="yewu_top_box">
                        <div class="image_box col-md-6">
                            <img src="img/b2.jpg" alt="">
                        </div>
                        <p class="col-md-6">公司提供：化学研究的科学插图的作品和动画的截图作品与以上所述作品的制作方法的信息。<br>提供化学动画制作方法的信息的收费标准是：每提供一种化学动画的制作方法的信息收费为500元-3500元，详情咨询客服。</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="yewu_top_box">
                        <div class="image_box col-md-6">
                            <img src="img/b1.jpg" alt="">
                        </div>
                        <p class="col-md-6">公司提供：高尔夫球的训练方法的动画的截图与高尔夫球的训练要领的介绍。<br>提供动画制作方法的信息的收费标准是：每提供一种高尔夫球的训练方法的动画制作的方法的信息收费为500元-3500元，详情咨询客服。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
       <!--主营业务-->
    <div class="yewu_box" id="yewu">
        <div class="yewu_content container">
            <!--<div class="item">-->
                <!--<h2>主营业务</h2>-->
            <!--</div>-->
            <div class="yewu-bottom row">
                <!--1-->
                <div class="con1 col-md-4">
                    <div class="con_box1">
                        <!--下拉框-->
                        <div class="con_box2 haha">
                            <div class="box2_img">
                                <img src="img/yewu2.jpg" alt="">
                                <div class="zhezhao">
                                    <div class="zhezhao_font">
                                        <h2>结构生物学图形与动画服务</h2>
                                        <p>
                                            随着计算机视觉和人工智能学科的发展,相关研究内容不断拓展、相互覆盖,图像理解既是对计算机视觉研究的延伸和拓展,又是人类智能的研究新领域,渗透着人工智能的研究进程。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="box2_icon">
                                <div class="icon_box">
                                    <div class="icon1">
                                        <img src="img/fengfu.png" alt="" class="img">
                                        <span>图像处理的多样性</span>
                                    </div>
                                    <div class="icon1">
                                        <img src="img/jingzhun.png" alt="" class="img">
                                        <span>图像的精度和正确性</span>
                                    </div>
                                    <div class="icon1">
                                        <img src="img/zonghe.png" alt="" class="img">
                                        <span>技术的交叉和综合</span>
                                    </div>
                                    <div class="icon1">
                                        <img src="img/data.png" alt="" class="img">
                                        <span>数据处理量大</span>
                                    </div>
                                </div>
                            </div>
                            <div class="box2_impor">
                                <div class="span">平台正在支持/提供：</div>
                                <div class="impor_imgs">
                                    <span>医学影像</span>
                                    <span>图像处理</span>
                                </div>
                                <div class="span"><a href="#" class="enter">进入>></a></div>

                            </div>
                        </div>
                        <div class="ai">
                            <div class="con_first">
                                <img src="img/yewu2.jpg" alt="">
                            </div>
                            <h2>结构生物学图形与动画服务</h2>
                            <p> 生物学与医学的应用：蛋白质的结构预测，蛋白质与蛋白质的相互作用的预测，医学影像学，药物虚拟筛选，基因组医学</p>
                            <p> 工业应用：机器故障预测</p>
                            <p> 金融应用：金融资产价格预测</p>
                            <p> 商业应用：商业知识图谱</p>
                        </div>

                    </div>
                </div>
                <!--2-->
                <div class="con1 col-md-4">
                    <div class="con_box1">
                        <!--下滑框-->
                        <div class="con_box2 haha">
                            <div class="box2_img">
                                <img src="img/yewu1.jpg" alt="">
                                <div class="zhezhao">
                                    <div class="zhezhao_font">
                                        <h2>虚拟现实相关产品</h2>
                                        <p>
                                            虚拟现实是多种技术的综合，包括实时三维计算机图形技术，对观察者头、眼和手的跟踪技术，以及触觉/力觉反馈、立体声、网络传输、语音输入输出技术等</p>
                                    </div>
                                </div>
                            </div>
                            <div class="box2_icon">
                                <div class="icon_box">
                                    <div class="icon1">
                                        <div></div>
                                        <img src="img/show.png" alt="" class="img">
                                        <span>显示/三维空间</span>
                                    </div>
                                    <div class="icon1">
                                        <div></div>
                                        <img src="img/shengyin.png" alt="" class="img">
                                        <span>声音/立体声</span>
                                    </div>
                                    <div class="icon1">
                                        <div></div>
                                        <img src="img/feel.png" alt="" class="img">
                                        <span>感觉/触觉</span>
                                    </div>
                                    <div class="icon1">
                                        <div></div>
                                        <img src="img/yuyin.png" alt="" class="img">
                                        <span>语音/自然语言</span>
                                    </div>
                                </div>
                            </div>
                            <div class="box2_impor">
                                <div class="span">平台正在支持/提供：</div>
                                <div class="impor_imgs">
                                    <span>工业仿真</span>
                                    <span>生物医学</span>
                                </div>
                                <div class="span"><a href="#" class="enter">进入>></a></div>

                            </div>
                        </div>
                        <div class="ai">
                            <div class="con_first">
                                <img src="img/yewu1.jpg" alt="">
                            </div>
                            <h2>虚拟现实相关产品</h2>

                        </div>

                        <!--<p>医学、娱乐，军事航天、室内设计、房产开发、工业仿真</p>-->
                        <!--<p>Web3D、游戏、康复训练、数字地球、教育、培训实训、生物力学</p>-->
                        <!--<div class="yewu_img">-->
                        <!--<div class="img_box">-->
                        <!--<span>多感知性</span>-->
                        <!--&lt;!&ndash;<img src="img/ye1.jpg" alt="">&ndash;&gt;-->
                        <!--</div>-->
                        <!--<div class="img_box">-->
                        <!--<span>存在感</span>-->
                        <!--&lt;!&ndash;<img src="img/ye1.jpg" alt="">&ndash;&gt;-->
                        <!--</div>-->
                        <!--<div class="img_box">-->
                        <!--<span>交互性</span>-->
                        <!--&lt;!&ndash;<img src="img/ye1.jpg" alt="">&ndash;&gt;-->
                        <!--</div>-->
                        <!--<div class="img_box">-->
                        <!--<span>自主性</span>-->
                        <!--&lt;!&ndash;<img src="img/ye1.jpg" alt="">&ndash;&gt;-->
                        <!--</div>-->
                        <!--</div>-->
                    </div>

                </div>
                <!--3-->
                <div class="con1 col-md-4">
                    <div class="con_box1">
                        <div class="con_box2 haha">
                            <div class="box2_img">
                                <img src="img/yewu3.jpg" alt="">
                                <div class="zhezhao">
                                    <div class="zhezhao_font">
                                        <h2>办公自动化与电子商务</h2>
                                        <p>
                                            试图找出自然语言的规律，建立运算模型，最终让电脑能够像人类般分析，理解和处理自然语言。这一种是计算机科学与人工智能领域正旺的科技领域，是人工智能的难点和重点之一。</p>
                                    </div>
                                </div>
                            </div>
                            <div class="box2_icon">
                                <div class="icon_box">
                                    <div class="icon1">
                                        <img src="img/jiansuo.png" alt="" class="img">
                                        <span>智能文本检索</span>
                                    </div>
                                    <div class="icon1">
                                        <img src="img/chouqu.png" alt="" class="img">
                                        <span>智能文本抽取</span>
                                    </div>
                                    <div class="icon1">
                                        <img src="img/guolv.png" alt="" class="img">
                                        <span>智能文本过滤</span>
                                    </div>
                                    <div class="icon1">
                                        <img src="img/fuwu.png" alt="" class="img">
                                        <span>智能文本服务</span>
                                    </div>
                                </div>
                            </div>
                            <div class="box2_impor">
                                <div class="span">平台正在支持/提供：</div>
                                <div class="impor_imgs">
                                    <span>积件</span>
                                    <span>实用化</span>
                                </div>
                                <div class="span"><a href="#" class="enter">进入>></a></div>

                            </div>
                        </div>
                        <div class="ai">
                            <div class="con_first">
                                <img src="img/yewu3.jpg" alt="">
                            </div>
                            <h2>办公自动化与电子商务</h2>
                        </div>

                        <!--<p>智能是一个感觉、回忆、思维、语言、行为的智能化过程</p>-->
                        <!--<p>文本是计算机的一种文档类型，是记载和储存文字信息的一种工具</p>-->
                        <!--<div class="yewu_img">-->
                        <!--<div class="img_box">-->
                        <!--<span>信息检索</span>-->
                        <!--</div>-->
                        <!--<div class="img_box">-->
                        <!--<span>信息抽取</span>-->
                        <!--</div>-->
                        <!--<div class="img_box">-->
                        <!--<span>信息过滤</span>-->
                        <!--</div>-->
                        <!--<div class="img_box">-->
                        <!--<span>信息服务</span>-->
                        <!--</div>-->
                        <!--</div>-->
                    </div>

                </div>
            </div>
        </div>
    </div>
   
     <!--软件与服务2-->
    <div class="service_two">
    	
    	
        <script type="text/javascript">
        
//         	$(function(){
//         		 $.ajax({
//   	        	   type: "GET",
//   	        	   url: "productTypeAction_getExamples",
//   	        	 success: function(msg){
//   	        	     alert( "Data Saved: " + msg );
//   	        	   }
//   	           });
        
        
        </script>
     <c:forEach items="${list }" var="types" varStatus="in">
	        <h2>${types.tname }</h2>
	        <div class="container serviceOne">
	            <div class="col-md-3">
	                <img src="img/mianbaoxie.png" alt="">
			                <ul class="first">
	    				<c:forEach items="${types.children}" var="s"> 
			                	<li > <div onmouseover="javascript:firstFunc(${s.tid});">${s.tname}</div></li>
		                
	            		</c:forEach>
			                </ul>
	            </div>
	            <div class="col-md-9">
	            
	            
		        		<c:forEach items="${types.children }" var="s">
	                <ul class="second block">
		        		<c:forEach items="${s.children }" var="ss">
		        				<li class="second-li"><div onmouseover="javascript:secondFunc(${ss.tid });">${ss.tname}</div></li>
	                   </c:forEach>
		                  <ul class="third">
		                     <c:forEach items="${s.children}" var="sa" varStatus="in">
	                    		<c:forEach items="${sa.children }" var="ss">
	                    			<c:choose>
								<c:when test="${(in.index+1)}==1 ">
		                       			 <li  class="block">									<!-- 第三等级 第二等级的展示 -->
			                         		<span class="third-span"> 
			                                		<a href="productAction_typeProduct.action?tid=${ss.tid}"><div>${ss.tname }</div></a>
				                                	<c:forEach items="${ss.children }" var="sss">			<!-- 第四等级  -->
				                                   		<a href="productAction_detailProduct.action?tid=${sss.tid}"><span>${sss.tname}</span></a>
				                                    </c:forEach>
			                                </span>
		                        		</li>
		                       		 </c:when>
		                        		<c:otherwise>
			                        		<li>									<!-- 第三等级 第二等级的展示 -->
				                         		<span class="third-span"> 
				                                	<a href="productAction_typeProduct.action?tid=${ss.tid}"><div> ${ss.tname }</div></a>
					                                	<c:forEach items="${ss.children }" var="sss">			<!-- 第四等级  -->
					                                   		<a href="productAction_detailProduct.action?tid=${sss.tid}"><span>${sss.tname}</span></a>
					                                    </c:forEach>
				                                </span>
			                        		</li>
		                        		</c:otherwise>
		                        		</c:choose>
		                        	</c:forEach>
	              			  </c:forEach>
		                    </ul>
	               		 </ul>
	                </c:forEach>
	            </div>
	        </div>
		</c:forEach>
			
			
			
     <c:forEach items="${listSec}" var="types" varStatus="in">
	        <h2>${types.tname }</h2>
	        <div class="container serviceTwo">
	            <div class="col-md-3">
	                <img src="img/mianbaoxie.png" alt="">
			                <ul class="first">
	    				<c:forEach items="${types.children}" var="s">   <!-- 教育 工业 -->
			                	<li > <div onmouseover="javascript:firstFunc(${s.tid });">${s.tname}</div></li>
		                </c:forEach>
			                </ul>
	            </div>
	            <div class="col-md-9">
<%-- 		        		<jsp:include page="right.jsp" flush="true" > --%>
<%-- 		        		<%@ include file="right.jsp" %> --%>

						<c:forEach items="${types.children }" var="s">
	                <ul class="second block">
		        		<c:forEach items="${s.children }" var="ss">
		        				<li class="second-li"><div onmouseover="javascript:secondFunc(${ss.tid });">${ss.tname}</div></li>
	                   </c:forEach>
		                  <ul class="third">
		                     <c:forEach items="${s.children}" var="sa" varStatus="in">
	                    	
	                    			<c:choose>
								<c:when test="${(in.index+1)}==1 ">
		                       			 <li  class="block">	
		                       			 	<c:forEach items="${sa.children }" var="ss">								<!-- 第三等级 第二等级的展示 -->
			                         		<span class="third-span"> 
			                                		<a href="productAction_typeProduct.action?tid=${ss.tid}"><div> ${ss.tname }</div></a>
				                                	<c:forEach items="${ss.children }" var="sss">			<!-- 第四等级  -->
				                                   		<a href="productAction_detailProduct.action?tid=${sss.tid}"><span>${sss.tname}</span></a>
				                                    </c:forEach>
			                                </span>
			                                </c:forEach>
		                        		</li>
		                       		 </c:when>
		                        		<c:otherwise>
			                        		<li>	
			                        			<c:forEach items="${sa.children }" var="ss">								<!-- 第三等级 第二等级的展示 -->
				                         		<span class="third-span"> 
				                                	<a href="productAction_typeProduct.action?tid=${ss.tid}"><div> ${ss.tname }</div></a>
					                                	<c:forEach items="${ss.children }" var="sss">			<!-- 第四等级  -->
					                                   		<a href="productAction_detailProduct.action?tid=${sss.tid}"><span>${sss.tname}</span></a>
					                                    </c:forEach>
				                                </span>
				                                </c:forEach>
			                        		</li>
		                        		</c:otherwise>
		                        		</c:choose>
		                        	</c:forEach>
	              	
		                    </ul>
	               		 </ul>
	                </c:forEach>
							
	            </div>
	        </div>
	</c:forEach>
  </div>
    
    
    <!--高尔夫器材-->
    <div class="shop_box">
        <h4>高尔夫辅助器材与软件</h4>
        <div class="shop_module">
            <div class="container">
                <div class="col-md-4">
                    <a href="shop.jsp">
                        <img src="img/g4.jpg" alt="">
                        <div class="zhe1">
                            <img src="img/jia.png" alt="">
                        </div>
                    </a>
                    <a href="shop.jsp">
                        <h5>高尔夫训练器材</h5>
                        <p>这款高尔夫球的训练器材的外包装基本定了尺寸大小，30*18*120，铝合金轮毂外包装，可以随身携带，高尔夫球爱好者可以把这款训练器材放在汽车的后背箱里带到高尔夫球的练习场，非常方便.</p>
                        <p>本产品分为教练版本与个人版本。本公司是这一运动器材的华北地区代理商。华北地区的高尔夫球爱好者可以向本公司购买这一器材，有购买意向者，请登录注册或者联系网站客服。</p>
                        <a href="shop.jsp" style="display: inline-block;text-align:right;padding-right: 20px;padding-bottom: 6px" class="cha_xiang">查看详情</a>
                    </a>
                </div>
                <div class="col-md-8">
                    <img src="img/g1.jpg" alt="" class="imgBack">
                    <div class="zhe">
                        <img src="img/play.png" alt="" data-src="videos/1-1.mp4">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--播放界面-->
    <div class="video_zhezhao">
        <div class="videoplay_Box">
            <video src="" controls="controls" width="100%" height="100%" class="video"></video>
        </div>
        <img src="img/close.png" alt="">
    </div>
   
    <!--公司简介-->
    
</div>
<footer>
    <div class="footer_box">
        <div class="footer_content">
            <div class="footer_center">
                <div class="footer_first">
                    <div class="icon">华诚兴业</div>
                    <h2>北京华诚兴业软件开发有限责任公司</h2>
                    <div class="footer_p">
                        <p class="ai">Power Human with AI</p>
                        <p>为了人工智能终将创造的所有美好。</p>
                    </div>
                    <ul>
                        <li>
                            <a href="#" class="icon_weixin"></a>
                        </li>
                        <li>
                            <a href="#" class="icon_weibo"></a>
                        </li>
                        <li>
                            <a href="#" class="icon_twitter"></a>
                        </li>
                        <li>
                            <a href="#" class="icon_facebook"></a>
                        </li>

                    </ul>
                    <div class="copyright">
                        <p class="ps">北京市丰台区丰台北路32号院华胜写字楼A座107</p>
                        <p>Copyright © 2012-2017 北京华诚兴业软件开发有限责任公司,<a href="http://www.miibeian.gov.cn" target="_Blank"
                                                                     style="color:#707070">京ICP备17057156</a></p>
                    </div>
                </div>
                <div class="footer_second">
                    <h4>产品官网</h4>
                    <a href="#">Face++</a>
                    <a href="#">Face++国际站</a>
                    <a href="#">FaceID</a>
                </div>
                <div class="footer_third">
                    <h4>联系我们</h4>
                    <div class="contact_one">
                        <span class="shangwu">商务合作：</span>
                        <div class="right">
                            <div class="top">
                                <a href="#" class="email"></a>
                                <span>physicsart@163.com</span>
                            </div>
                            <div class="top">
                                <a href="#" class="iphone"></a>
                                <span>010-83667960</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script>window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')</script>
<script src="js/index.js"></script>
<!-- Swiper JS -->
<script src="js/swiper.min.js"></script>
<script>
    var mySwiper = new Swiper('.swiper-container', {
        autoplay: {
            delay: 2500,
            disableOnInteraction: true,
        },
//        autoplay : true,
        spaceBetween: 30,
        hashNavigation:
            {
                watchState: true,
            },
        pagination: {
            el: '.swiper-pagination',
            clickable:
                true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
    //    鼠标移入轮播悬停
    $('.swiper-slide').mouseenter(function () {
        mySwiper.autoplay.stop();
    })
    $('.swiper-slide').mouseleave(function () {
        mySwiper.autoplay.start();
    })
</script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?206a5bcb913ff3efc62aefbe2795773d";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>

</body>
</html>