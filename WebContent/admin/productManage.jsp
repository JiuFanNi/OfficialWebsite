<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/productManage.js"></script> 
<script type="text/javascript">
function formatpath(val,row){
	return "<img width=120 height=120 src='${pageContext.request.contextPath}/"+val+"'>";
}
$(function(){
	$('#Tname').combotree({
	    onSelect : function(node) {  
	        var tree = $(this).tree;  
	        //选中的节点是否为叶子节点,如果不是叶子节点,清除选中  
	        var isLeaf = tree('isLeaf', node.target);  
	        if (!isLeaf) {  
	            alert("只能选取子类型");
	            $('#Tname').treegrid("unselect");
	        }  
	    }  
	}); 
});
</script> 
</head>
<body style="margin:1px;">
	<table id="dg" title="商品管理" class="easyui-datagrid"
	 fitColumns="true" pagination="true" rownumbers="true"
	 url="productAction_list" fit="true" toolbar="#tb">
	 <thead>
	 	<tr>
	 		<th field="cb" checkbox="true" align="center"></th>
	 		<th field="id" width="30" align="center">编号</th>
	 		<th field="path" width="160" align="center"  formatter="formatpath">产品图片</th>
	 		<th field="name" width="70" align="center">产品名称</th>
	 		<th field="price" width="50" align="center">价格</th>
	 		<th field="productpath" width="50" align="center" hidden="true">产品exe</th>
	 	 	<th field="type.tid" width="50" align="center" formatter="typeid" hidden="true" >所属商品类id</th>
	 		<th field="type.tname" width="50" align="center" formatter="typename">所属商品类</th> 
	 		<th field="description" width="100" align="center" >描述</th>
	 	</tr>
	 </thead>
	</table>
	
	<div id="tb">
		<div>
			<a href="javascript:openProductAddDialog()" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
			<a href="javascript:openProductModifyDialog()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">修改</a>
			<a href="javascript:deleteProduct()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
		</div>
	</div>
	<div id="dlg" class="easyui-dialog" style="width:520px;height:360px;padding: 10px 20px"
	  closed="true" buttons="#dlg-buttons">
	 	<form id="fm" method="post" enctype="multipart/form-data">
	 		<table cellspacing="8px">
	 			<tr>
	 				<td>产品名称：</td>
	 				<td colspan="4"><input type="text" id="name" name="product.name" class="easyui-validatebox" required="true" style="width: 300px"/></td>
	 			</tr>
	 			<tr>
	 				<td>价格：</td>
	 				<td colspan="4"><input type="text" id="price" name="product.price" class="easyui-validatebox" required="true"/></td>
	 			</tr>
	 			<tr>
	 				<td>产品图片：</td>
	 				<td colspan="4"><input type="file" id="pt" name="path" multiple/></td>
	 			</tr>
	 			<tr>
	 				<td>产品文件：</td>
	 				<td colspan="4"><input type="file" id="ppt" name="productpath" /></td>
	 			</tr>
	 			<tr>
	 				<td>所属类：</td>
	 				<td><input id="Tname"  class="easyui-combotree" style="width:350px" name="product.type.tid"  /></td>
	 			</tr>
	 			<tr>
	 				<td valign="top">详情备注：</td>
	 				<td colspan="4">
	 					<textarea rows="5" cols="50" id="description" name="product.description"></textarea>
	 					<input type="hidden" id="path" name="product.path"/>
	 					<input type="hidden" id="productpath" name="product.productpath"/>
	 				</td>
	 			</tr>
	 		</table>
	 	</form>
	</div>
	
	<div id="dlg-buttons">
		<a href="javascript:saveProduct()" class="easyui-linkbutton" iconCls="icon-ok">保存</a>
		<a href="javascript:closeProductDialog()" class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
	</div>
</body>
</html>