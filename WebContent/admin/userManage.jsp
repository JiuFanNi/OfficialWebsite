<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>用户管理</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/userManage.js"></script>
</head>
<body  style="margin:1px;">	
	<table id="dg" title="用户管理" url="admAction_getAdmList"  class="easyui-datagrid" fitColumns="true" pagination="true" rownumbers="true" fit="true"  toolbar="#tb">
	 <thead>
	 	<tr>
	 		<th field="cb" checkbox="true" align="center"></th>
	 		<th field="aid" width="50" align="center">编号</th>
	 		<th field="admName" width="60" align="center">真实姓名</th>
	 		<th field="admPassword" width="70" align="center">密码</th>
	 		<th field="admEmail" width="70" align="center">邮件</th>
	 		<th field="admMoble" width="70" align="center">联系电话</th>
	 		<th field="state" width="50" align="center">状态</th>
	 		<th field="code" width="70" align="center">验证码</th>
	 	</tr>
	 </thead>
	</table>
	
  	<div id="tb">
		<div>
			<a href="javascript:openUserAddDialog()" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
			<a href="javascript:openUserModifyDialog()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">修改</a>
			<a href="javascript:deleteUser()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
		</div>
	
	</div>	
	<div id="dlg" class="easyui-dialog" style="width: 570px;height:330px;padding: 10px 20px"
	  closed="true" buttons="#dlg-buttons" >
	 	<form id="fm" method="post">
	 		<table cellspacing="8px">
	 			<tr>
	 				<td>真实姓名：</td>
	 				<td><input type="text" id="admName" name="adm.admName" class="easyui-validatebox" required="true"/></td>
	 			<tr>
	 				<td>密码：</td>
	 				<td><input type="text" id="admPassword" name="adm.admPassword" class="easyui-validatebox" required="true"/></td>
	 			</tr>
	 			<tr>
	 				<td>邮件：</td>
	 				<td><input type="text" id="admEmail" name="adm.admEmail" class="easyui-validatebox" validType="email" required="true"/></td>
	 				<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	 				<td>联系电话：</td>
	 				<td><input type="text" id="admMoble" name="adm.admMoble" class="easyui-validatebox" required="true"/></td>
	 			</tr>
	 			<tr>
	 				<td>状态：</td>
	 				<td><input type="text" id="state" name="adm.state" class="easyui-validatebox" required="true"/></td>
	 			</tr>
	 		</table>
	 	</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:saveUser()" class="easyui-linkbutton" iconCls="icon-ok">保存</a>
		<a href="javascript:closeUserDialog()" class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
	</div>
</body>
</html>