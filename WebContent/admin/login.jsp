<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>管理员登录</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/normalize.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/demo.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/component.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/fonts/font-awesome-4.2.0/css/font-awesome.min.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/js/classie.js"></script>
</head>
<body>
<form action="userAction" method="post">
	<section class="content" border="1px">
	<span class="input input--hideo">
					<input class="input__field input__field--hideo" type="text" id="userName" name="userName" placeholder="用户名" />
					<label class="input__label input__label--hideo" for="userName">
						<i class="fa fa-fw fa-user icon icon--hideo"></i>
						<span class="input__label-content input__label-content--hideo">用户名</span>
					</label>
				</span><br/>
		<span class="input input--hideo">
					<input class="input__field input__field--hideo" type="text" id="userPassword" name="userPassword" placeholder="密码"/>
					<label class="input__label input__label--hideo" for="userPassword">
						<i class="fa fa-fw fa-lock icon icon--hideo"></i>
						<span class="input__label-content input__label-content--hideo">密码</span>
					</label>
				</span>
	</section>
		<input class="btn btn-primary" type="submit" name="submit"/>
	
	</form>
</body>
</html>