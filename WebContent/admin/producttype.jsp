<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/producttype.js"></script>

</head>
<body>

	<table id="dg" title="商品大类管理" class="easyui-datagrid"
	 fitColumns="true" pagination="true" rownumbers="true"
	 url="productTypeAction_gettype" fit="true" toolbar="#tb">
	 <thead>
	 	<tr>
	 		<th field="cb" checkbox="true" align="center"></th>
	 		<th field="tid" width="50" align="center" formatter="typetid">编号</th>
	 		<th field="tname" width="60" align="center">类型名</th>
	 		<th field="pid" width="50" align="center"  formatter="typepid">pid</th>
	 	</tr>
	 </thead>
	</table>
	
	<div id="tb">
		<div>
			<a href="javascript:openproducttypeAddDialog()" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
			<a href="javascript:openproducttypeModifyDialog()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">修改</a>
<!-- 			<a href="javascript:deleteproducttype()" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a> -->
		</div>
	</div>
	<!-- dlg打开窗口 -->
		<div id="dlg" class="easyui-dialog" style="width: 570px;height:260px;padding: 10px 20px"
	  closed="true" buttons="#dlg-buttons" >
	  <!-- 调到fm表单 -->
	 	<form id="fm" method="post">
	 		<table cellspacing="8px">
	 			<tr>
	 				<td>id:</td>
	 				<td><input type="text" id="tid" readonly="readonly"  required="true"/></td>
	 				<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	 			</tr>
	 			<tr>
	 				<td>父节点:</td>
						<td><input id="Tname" style="width:350px" class="easyui-combotree" name="s_type.parentType.tid"  />
						</td>	
 				</tr>
 				<tr>
	 				<td>类型名:</td>
	 				<td><input type="text" id="tname" name="s_type.tname" class="easyui-validatebox" required="true"/></td>
	 				<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	 			</tr>
	 		</table>
	 	</form>
	</div>
	<div id="dlg-buttons">
		<a href="javascript:saveproducttype()" class="easyui-linkbutton" iconCls="icon-ok">保存</a>
		<a href="javascript:closeproducttypeDialog()" class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
	</div>	
	<div id="win" class="easyui-window" closed="true" style="width:300px;height:180px;">
	<form  id="fm2" method="post">
		<table cellspacing="8px">
	 			<tr>
	 				<td>tid:</td>
	 				<td><input type="text" id="testid" readonly="readonly" name="tid" class="easyui-validatebox" required="true"/></td>
	 				<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	 				
	 			</tr>
	 			<tr>
	 				<td>pid:</td>
	 				<td><input type="text" id="testpid" readonly="readonly" name="s_type.parentType.tid" class="easyui-validatebox"  required="true"/></td>
	 				<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
 				</tr>
 				<tr>
	 				<td>类型名:</td>
	 				<td><input type="text" id="testtname" name="s_type.tname" class="easyui-validatebox" required="true"/></td>
	 				<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	 			</tr>
	 		</table>
		<div style="padding:5px;text-align:center;">
			<a href="javascript:saveproducttype2()" class="easyui-linkbutton" icon="icon-ok">保存</a>
		</div>
	</form>
</div>
</body>
</html>