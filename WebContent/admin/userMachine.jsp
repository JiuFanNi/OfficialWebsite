<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
var url;
function admName(val,row){
	return row.adm.admName;
}
function admEmail(val,row){
	return row.adm.admEmail;
}
function pid(val,row){
	return row.product.id;
}

function openUserMachineAddDialog(){
	$("#dlgAdd").dialog("open").dialog("setTitle","添加用户信息");
	url="userMachineAction_add.action";
}


function openUserMachineModifyDialog(){
	var selectedRows=$("#dg").datagrid('getSelections');
	if(selectedRows.length!=1){
		$.messager.alert("系统提示","请选择一条要编辑的数据！");
		return;
	}
	var row=selectedRows[0];
	$("#dlg").dialog("open").dialog("setTitle","编辑商品信息");
	$("#name").val(row.adm.admName);
	$("#email").val(row.adm.admEmail);
	$("#mcode").val(row.MCode);
	$("#pid").combobox("setValue",row.product.id);
	url="userMachineAction_save.action?s_userMachine.id="+row.id;
}
function saveUserMachine(){
	$("#fm").form("submit",{
		url:url,
		onSubmit:function(){
			if($('#pid').combobox("getValue")==""){
				$.messager.alert("系统提示","请选择商品名称");
				return false;
			} 
			return $(this).form("validate");
		},
		
		success:function(result){
			var result=eval('('+result+')');
			if(result.success){
				$.messager.alert("系统提示","保存成功");
				resetValue();
				$("#dlg").dialog("close");
				$("#dg").datagrid("reload");
			}else{
				$.messager.alert("系统提示","保存失败");
				return;
			}
		}
	});
	
}
function save(){
	$("#fmAdd").form("submit",{
		url:url,
		onSubmit:function(){
			if($('#pids').combobox("getValue")==""){
				$.messager.alert("系统提示","请选择商品名称");
				return false;
			} 
			return $(this).form("validate");
		},
		
		success:function(result){
			var result=eval('('+result+')');
			if(result.success){
				$.messager.alert("系统提示","保存成功");
				resetAddValue();
				$("#dlgAdd").dialog("close");
				$("#dg").datagrid("reload");
			}else{
				$.messager.alert("系统提示",result.error);
				return;
			}
		}
	});
	
}
function resetValue(){
	$("#name").val("");
	$("#email").val("");
	$("#mcode").val("");
	$("#pid").combobox("setValue","");
}
function resetAddValue(){
	$("#names").val("");
	$("#emails").val("");
	$("#mcodes").val("");
	$("#pids").combobox("setValue","");
}

function closeUserMachineDialog(){
	$("#dlg").dialog("close");
	resetValue();
}
function close(){
	$("#dlgAdd").dialog("close");
	resetAddValue();
}

</script>
</head>
<body>


	<table id="dg" title="用户产品管理" class="easyui-datagrid"
	 fitColumns="true" pagination="true" rownumbers="true"
	 url="userMachineAction_list.action" fit="true" toolbar="#tb">
	 <thead>
	 	<tr>
	 		<th field="cb" checkbox="true" align="center"></th>
	 		<th field="id" width="30" align="center">编号</th>
	 		<th field="adm.admName" width="50" align="center" formatter="admName">用户名</th>
	 	 	<th field="adm.admEmail" width="50" align="center" formatter="admEmail">用户邮箱</th>
	 		<th field="product.id" width="50" align="center" formatter="pid">产品编号</th> 
	 		<th field="MCode" width="100" align="center" >机器识别码</th>
	 	</tr>
	 </thead>
	</table>
	
	
	<div id="tb">
		<div>
			<a href="javascript:openUserMachineModifyDialog()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">修改</a>
			<a href="javascript:openUserMachineAddDialog()" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
		</div>
	</div>
	
	
	<div id="dlg" class="easyui-dialog" style="width:520px;height:300px;padding: 10px 20px"
	  closed="true" buttons="#dlg-buttons">
	 	<form id="fm" method="post" >
	 		<table cellspacing="8px">
	 			<tr>
	 				<td>用户名：</td>
	 				<td colspan="4"><input type="text" id="name" readonly="readonly" class="easyui-validatebox"  style="width: 300px"/></td>
	 			</tr>
	 			<tr>
	 				<td>用户邮箱：</td>
	 				<td colspan="4"><input type="text" id="email" readonly="readonly"  class="easyui-validatebox"/></td>
	 			</tr>
	 			<tr>
	 				<td>机器识别码：</td>
	 				<td colspan="4"><input type="text" id="mcode" name="Mcodes"/></td>
	 			</tr>
	 			<tr>
	 				<td>产品编号：</td>
	 				<td colspan="4">
	 				<input class="easyui-combobox" id="pid" name="s_userMachine.product.id" data-options="panelHeight:'auto',editable:false,valueField:'id',textField:'name',url:'productAction_getAllProduct.action'"/>
	 				</td>
	 			</tr>
	 		</table>
	 	</form>
	</div>
	
	
	<div id="dlgAdd" class="easyui-dialog" style="width:520px;height:300px;padding: 10px 20px"
	  closed="true" buttons="#dlg-button">
	 	<form id="fmAdd" method="post" >
	 		<table cellspacing="8px">
	 			<tr>
	 				<td>用户名：</td>
	 				<td colspan="4"><input type="text" id="names" name="s_userMachine.adm.admName" class="easyui-validatebox"  style="width: 300px"/></td>
	 			</tr>
	 			<tr>
	 				<td>用户邮箱：</td>
	 				<td colspan="4"><input type="text" id="emails"  name="s_userMachine.adm.admEmail"  class="easyui-validatebox"/></td>
	 			</tr>
<!-- 	 			<tr> -->
<!-- 	 				<td>机器识别码：</td> -->
<!-- 	 				<td colspan="4"><input type="text" id="mcodes" name="Mcode"  /></td> -->
<!-- 	 			</tr> -->
	 			<tr>
	 				<td>产品编号：</td>
	 				<td colspan="4">
	 				<input class="easyui-combobox" id="pids" name="s_userMachine.product.id" data-options="panelHeight:'auto',editable:false,valueField:'id',textField:'name',url:'productAction_getAllProduct.action'"/>
	 				</td>
	 			</tr>
	 		</table>
	 		
	 	</form>
	</div>
	
	
	<div id="dlg-buttons">
		<a href="javascript:saveUserMachine()" class="easyui-linkbutton" iconCls="icon-ok">保存</a>
		<a href="javascript:closeUserMachineDialog()" class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
	</div>
	<div id="dlg-button">
		<a href="javascript:save()" class="easyui-linkbutton" iconCls="icon-ok">保存</a>
		<a href="javascript:close()" class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
	</div>
</body>
</html>