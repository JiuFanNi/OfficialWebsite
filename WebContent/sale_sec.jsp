<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>shop</title>
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1,minimum-scale=1">
<script src="js/jquery-1.11.0.min.js"></script>
<script src="js/index.js"></script>
<link rel="icon" type="image/png" href="img/logoH.png" sizes="96*96">
</head>
<body>
<%@include file="header.jsp" %>
<c:choose>
<c:when test="${not empty list}">
<c:forEach items="${list}" var="p">
<div class="top_box">
   <div class="container">
       <div class="col-md-12">
           <div>${p.type.tname}</div>
           <span><a href="productAction_download.action?productfile=${p.productpath}">下载</a></span>
       </div>
   </div>
</div>
<div class="sale_sec_box">
    <div class="container">
        <div class="col-md-4">
            <img src="${p.path}" alt=""><br>
            <span>￥${p.price}</span><br>
            <span>Version: 2017.3.2</span><br>
            <span> Build: 173.4127.31</span><br>
            <span> Released: December 27, 2017</span>
        </div>
        <div class="col-md-8">
            <p>${p.description}</p>
          <p>JetBrains针对个人开发者及企业组织提供不同的授权方式。我们提供多种付款方式，包含银联卡、各种国际信用卡、PayPal及支付宝。企业户也可通过银行汇款的方式完成付款。 若您有任何购买或授权上的疑问，欢迎联系我们的中文销售代表为您服务。</p>
            <h1>Download ${p.name}</h1>
            <span class="spans">windows</span>
        </div>
    </div>
</div>
</c:forEach>
</c:when>
<c:otherwise>
<div class="sale_sec_box" style="min-height:524px">
<h3 align="center">暂无软件</h3>
</div>
</c:otherwise>
</c:choose> 
<footer>
    <div class="footer_box">
        <div class="footer_content">
            <div class="footer_center">
                <div class="footer_first">
                    <div class="icon">华诚兴业</div>
                    <h2>北京华诚兴业软件开发有限责任公司</h2>
                    <div class="footer_p">
                        <p class="ai">Power Human with AI</p>
                        <p>为了人工智能终将创造的所有美好。</p>
                    </div>
                    <ul>
                        <li>
                            <a href="#" class="icon_weixin"></a>
                        </li>
                        <li>
                            <a href="#" class="icon_weibo"></a>
                        </li>
                        <li>
                            <a href="#" class="icon_twitter"></a>
                        </li>
                        <li>
                            <a href="#" class="icon_facebook"></a>
                        </li>

                    </ul>
                    <div class="copyright">
                        <p class="ps">北京市丰台区丰台北路32号院华胜写字楼A座107</p>
                        <p>Copyright © 2012-2017 北京华诚兴业软件开发有限责任公司,<a href="http://www.miibeian.gov.cn" target="_Blank"
                                                                     style="color:#707070">京ICP备17057156</a></p>
                    </div>
                </div>
                <div class="footer_second">
                    <h4>产品官网</h4>
                    <a href="#">Face++</a>
                    <a href="#">Face++国际站</a>
                    <a href="#">FaceID</a>
                </div>
                <div class="footer_third">
                    <h4>联系我们</h4>
                    <div class="contact_one">
                        <span class="shangwu">商务合作：</span>
                        <div class="right">
                            <div class="top">
                                <a href="#" class="email"></a>
                                <span>physicsart@163.com</span>
                            </div>
                            <div class="top">
                                <a href="#" class="iphone"></a>
                                <span>010-83667960</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>