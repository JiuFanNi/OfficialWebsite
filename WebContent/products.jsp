<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1,minimum-scale=1">
    <title>产品</title>
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/products.js"></script>
    <link rel="stylesheet" href="css/products.css">
     <link rel="stylesheet" href="css/nstyle.css">
    <link rel="icon" type="image/png" href="img/logoH.png" sizes="96*96">
    <style>
        .main li:nth-child(4){
            background: #fff!important;
        }
        .main li:nth-child(4) a{
            color: #000000!important;
        }
    </style>
</head>
<body>
<%@include file="header.jsp" %> 
<div class="banner">
    <div class="banner_box"></div>
</div>
<div class="color">
    <!-- <div class="close"></div> -->
   <!-- <div class="openIframe"> --> 
       <!-- <iframe src="img/product0.jpg" frameborder="0" width="960" height="600" scrolling="NO" leftmargin="0" topmargin="0" ></iframe> --> 
    <!-- </div> -->
<div class="title">
    <h2>主要产品</h2>
</div>
</div>
<footer>
    <div class="footer_box">
        <div class="footer_content">
            <div class="footer_center">
                <div class="footer_first">
                    <div class="icon">华诚兴业</div>
                    <h2>北京华诚兴业软件开发有限责任公司</h2>
                    <div class="footer_p">
                        <p class="ai">Power Human with AI</p>
                        <p>为了人工智能终将创造的所有美好。</p>
                    </div>
                    <ul>
                        <li>
                            <a href="#" class="icon_weixin"></a>
                        </li>
                        <li>
                            <a href="#" class="icon_weibo"></a>
                        </li>
                        <li>
                            <a href="#" class="icon_twitter"></a>
                        </li>
                        <li>
                            <a href="#" class="icon_facebook"></a>
                        </li>

                    </ul>
                    <div class="copyright">
                        <p class="ps">北京市丰台区丰台北路32号院华胜写字楼A座107</p>
                        <p>Copyright © 2012-2017 北京华诚兴业软件开发有限责任公司,<a href="http://www.miibeian.gov.cn" target="_Blank"
                                                                     style="color:#707070">京ICP备17057156</a></p>
                    </div>
                </div>
                <div class="footer_second">
                    <h4>产品官网</h4>
                    <a href="#">Face++</a>
                    <a href="#">Face++国际站</a>
                    <a href="#">FaceID</a>
                </div>
                <div class="footer_third">
                    <h4>联系我们</h4>
                    <div class="contact_one">
                        <span class="shangwu">商务合作：</span>
                        <div class="right">
                            <div class="top">
                                <a href="#" class="email"></a>
                                <span>physicsart@163.com</span>
                            </div>
                            <div class="top">
                                <a href="#" class="iphone"></a>
                                <span>010-83667960</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>