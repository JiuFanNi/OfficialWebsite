package com.webTest.Dao;

import java.util.List;

import com.webTest.Bean.Adm;
import com.webTest.Bean.PageBean;
import com.webTest.Bean.UserMachine;

public interface UserMachineDao {

	List<UserMachine> getProd(UserMachine userMachine);

	void addM_code(UserMachine userMachine2);

	boolean findM_code(UserMachine userMachine2);
	
	List<UserMachine> usermachinelist(UserMachine s_userMachine,PageBean pageBean);
	
	Long getusermachineCount(UserMachine s_userMachine);
	void upadateUserMachine(UserMachine s_userMachine);

	Adm findAdm(Adm adm);

	void add(UserMachine s_userMachine);
}
