package com.webTest.Dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.webTest.Bean.Adm;
import com.webTest.Bean.PageBean;
import com.webTest.Bean.UserMachine;

@Repository("userMachineDao")
public class UserMachineDaoImpl implements UserMachineDao {

	private Session session;  
	public  SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Session getCurrentSession() {  
        if(session!=null)  
            return session;  
        return sessionFactory.getCurrentSession();  
    }  
	
	
	public List<UserMachine> getProd(UserMachine userMachine) {
		String hql="from UserMachine where aid=? and pid=?";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter(0, userMachine.getAdm().getAid());
		query.setParameter(1, userMachine.getProduct().getId());
		List<UserMachine> list = query.list();
		if(list.size()>0 && !(list.isEmpty())){
			return list;
		}
		return null;
	}

	
	public void addM_code(UserMachine userMachine) {
		String hql="update UserMachine set m_code=? where aid=? and pid=? and id=?";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter(0, userMachine.getMCode());
		query.setParameter(1, userMachine.getAdm().getAid());
		query.setParameter(2, userMachine.getProduct().getId());
		query.setParameter(3, userMachine.getId());
		query.executeUpdate();
	}

	@Override
	public boolean findM_code(UserMachine userMachine) {
		String hql="from UserMachine where aid=? and pid=? and m_code=?";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter(0, userMachine.getAdm().getAid());
		query.setParameter(1, userMachine.getProduct().getId());
		query.setParameter(2, userMachine.getMCode());
		List<UserMachine> list = query.list();
		UserMachine u = list.get(0);
		System.out.println("----------"+u);
		if(null!=u) {
			return true;
		}
			return false;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<UserMachine> usermachinelist(UserMachine s_userMachine, PageBean pageBean) {
		List<UserMachine> list=new ArrayList<UserMachine>();
		String hql="from UserMachine";
		Query q=this.getCurrentSession().createQuery(hql);
		list=q.setFirstResult(pageBean.getStart()).setMaxResults(pageBean.getPageSize()).list();
		return list;
	}
	@Override
	public Long getusermachineCount(UserMachine userMachine) {
		String hql="SELECT COUNT(*) FROM UserMachine";
		Query q=this.getCurrentSession().createQuery(hql);
		Long long1=(Long) q.uniqueResult();
		return long1;
	}
	@Override
	public void upadateUserMachine(UserMachine s_userMachine) {
		String hql="update UserMachine set pid=?,m_code=? where id=? ";
		Query q=this.getCurrentSession().createQuery(hql);
		q.setParameter(0, s_userMachine.getProduct().getId());
		q.setParameter(1, s_userMachine.getMCode());
		q.setParameter(2, s_userMachine.getId());
		q.executeUpdate();
	}
	@Override
	public Adm findAdm(Adm adm) {
		String hql="from Adm where admName=? and admEmail=?";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter(0, adm.getAdmName());
		query.setParameter(1, adm.getAdmEmail());
		
		List<Adm> list = query.list();
		if(list.size()>0 && !(list.isEmpty())){
			return list.get(0);
		}
		return null;
	}
	@Override
	public void add(UserMachine s_userMachine) {
		this.getCurrentSession().save(s_userMachine);
		
	}

}
