package com.webTest.Dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.webTest.Bean.PageBean;
import com.webTest.Bean.Type;

@Repository("producttypeDao")
public class ProductTypeDaoImpl implements ProductTypeDao {
	private Session session;  
	
	public  SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Session getCurrentSession() {  
        if(session!=null)  
            return session;  
        return sessionFactory.getCurrentSession();  
    }  
	

	@Override
	@SuppressWarnings("unchecked")
	public List<Type> types(Type s_type, PageBean pageBean) {
		List<Type> list=new ArrayList<Type>();
		String hql="from Type";
		Query q=this.getCurrentSession().createQuery(hql);
		list=q.setFirstResult(pageBean.getStart()).setMaxResults(pageBean.getPageSize()).list();
		return list;
	}

	@Override
	public Long gettypecount(Type s_type) {
		String hql="SELECT COUNT(*) FROM Type ";
		Query q = this.getCurrentSession().createQuery(hql);  
		Long long1=(Long) q.uniqueResult();
		return long1;
	}
	
	
	
	@Override
	public List<Type> getFirst() {
		String hql="FROM Type WHERE tid=1";
		List<Type> list= this.getCurrentSession().createQuery(hql).list();
		return list;
	}
	
	
	@Override
	public List<Type> getMouseOver(int tid) {
		String hql="FROM Type WHERE tid=?";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter(0, tid);
		List<Type> list= query.list();
		return list;
	}
	
	@Override
	public void updateType(Type type) {
		this.getCurrentSession().update(type);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Type> parentsType() {
		String hql="from Type where ParentType is null";
		List<Type> list=this.getCurrentSession().createQuery(hql).list();
		return list;
	}
	@Override
	public void insertType(Type type) {
		this.getCurrentSession().save(type);
		
	}
	@Override
	public void deleteType(Type type) {
		System.out.println(type);
		this.getCurrentSession().delete(type);
	}
	@Override
	public Type get(int tid) {
		Type type=new Type();
		type.setTid(tid);
		type=(Type) this.getCurrentSession().get(Type.class, type.getTid());
		return type;
	}
	@Override
	public List<Type> getSecond() {
		String hql="FROM Type WHERE tid=2";
		List<Type> list= this.getCurrentSession().createQuery(hql).list();
		return list;
	}


}
