package com.webTest.Dao;

import java.sql.SQLException;
import java.util.List;
import com.webTest.Bean.Adm;
import com.webTest.Bean.PageBean;
import com.webTest.Bean.UserMachine;

public interface AdmDao {
	
	 /**
	  * 后台查询用户
	  */
	 List<Adm> getAdmlist(Adm s_adm,PageBean pageBean);
	 Long getcount(Adm s_adm);
	 /**
	  * 用户登录
	  */
	 Adm get(String admEmail,String password);
	 /**
	  * 用户注册
	  */
	 boolean Register(Adm adm) throws Exception;
	 	 
	 void delet(Adm adm);
	 
	 Adm get(int aid);
	 
	 void addAdm(Adm adm);
	 
	 Adm findAdmByCode(String code) throws Exception;
	 
	 Adm findEmail(String admEmail) throws SQLException;

	 
}
