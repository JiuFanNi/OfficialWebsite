package com.webTest.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;


import com.webTest.Bean.Adm;
import com.webTest.Bean.PageBean;
import com.webTest.Bean.Product;
import com.webTest.Bean.UserMachine;
@Repository("admDao")
public class AdmDaoImpl implements AdmDao {
	private Session session;  
	
	public  SessionFactory sessionFactory;
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public Session getCurrentSession() {  
        if(session!=null)  
            return session;  
        return sessionFactory.getCurrentSession();  
    }  
	
	
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Adm> getAdmlist(Adm s_adm, PageBean pageBean) {
		List<Adm> list=new ArrayList<Adm>();
		String hql="from Adm where flag=1";
		Query q=this.getCurrentSession().createQuery(hql);
		list=q.setFirstResult(pageBean.getStart()).setMaxResults(pageBean.getPageSize()).list();
		return list;
	}
	@Override
	public Long getcount(Adm s_adm) {
		String hql="SELECT COUNT(*) FROM Adm where flag=1";
	    Query q=this.getCurrentSession().createQuery(hql);
	    Long long1=(Long) q.uniqueResult();
		return long1;
	}

	
	@Override
	public Adm get(String admEmail,String password) {
		String hsql="from Adm a where a.admEmail='"+admEmail+"' and a.admPassword='"+password+"'";
		Query q = this.getCurrentSession().createQuery(hsql);  
		Adm result=(Adm)q.uniqueResult();
		return result;
	}


	@Override
	public Adm get(int aid) {
	Adm adm=new Adm();
	adm.setAid(aid);
	adm=(Adm) this.getCurrentSession().get(Adm.class,adm.getAid());
	return adm;
	}	


	@Override
	public void delet(Adm adm) {
		String hql = "update Adm set flag=0 where aid="+adm.getAid();
		this.getCurrentSession().createQuery(hql).executeUpdate();
//		this.getCurrentSession().delete(adm);
	}
	

	@Override
	public void addAdm(Adm adm) {
		this.getCurrentSession().merge(adm);
	}
	
	

	

	@Override
	public boolean Register(Adm adm) throws Exception {
		try {
			this.getCurrentSession().save(adm);
			Adm adm2 = get(adm.getAdmEmail(), adm.getAdmPassword());
			UserMachine u=new UserMachine();
			u.setAdm(adm2);
			Product p=new Product();
			p.setId(1);
			u.setProduct(p);
			this.getCurrentSession().save(u);
			return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		
	}
	
	@Override
	// DAO中 根据激活码查询用户的方法
	public Adm findAdmByCode(String code) throws Exception {
		// TODO Auto-generated method stub
		String hsql="from Adm a where a.code='"+code+"'";
		Query q = this.getCurrentSession().createQuery(hsql);  
		Adm result=(Adm)q.uniqueResult();
		return result;
	}

	@Override
	public Adm findEmail(String admEmail) throws SQLException {
		String hsql="from Adm a where a.admEmail= '"+admEmail+"' ";
		Query q = this.getCurrentSession().createQuery(hsql);  
		Adm result=(Adm)q.uniqueResult();
		return result;
	}

}
