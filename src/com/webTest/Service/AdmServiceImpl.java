package com.webTest.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webTest.Bean.Adm;
import com.webTest.Bean.PageBean;
import com.webTest.Bean.UserMachine;
import com.webTest.Dao.AdmDao;
import com.webTest.tools.MailUtils;
@Service("admService")
public class AdmServiceImpl implements AdmService {
	@Autowired
	private AdmDao admDao;

	
	public AdmDao getAdmDao() {
		return admDao;
	}


	public void setAdmDao(AdmDao admDao) {
		this.admDao = admDao;
	}


	@Override
	public List<Adm> getAdmlist(Adm s_adm, PageBean pageBean) {
		List<Adm> list=admDao.getAdmlist(s_adm, pageBean);
		return list;
	}
	
	@Override
	public Long getcount(Adm s_adm) {
		Long long1=admDao.getcount(s_adm);
		return long1;
	}

	
	@Override
	public Adm get(String admEmail,String password) {
		// TODO Auto-generated method stub
		return admDao.get(admEmail,password);
	}


	@Override
	public Adm get(int aid) {
		// TODO Auto-generated method stub
		Adm adm=admDao.get(aid);
		return adm;
	}



	@Override
	public void deletAdm(Adm adm) {
		admDao.delet(adm);
	
	}


	
	@Override
	public void addAdm(Adm adm) {
		admDao.addAdm(adm);
	}


	



	@Override
	public boolean Register(Adm adm)throws Exception {
		// TODO Auto-generated method stub
		MailUtils.sendMail(adm.getAdmEmail(), adm.getCode(), "来自华诚兴业的邮件");
		return admDao.Register(adm);
	}


	@Override
	public Adm findAdmByCode(String code) throws Exception {
		// TODO Auto-generated method stub
		return admDao.findAdmByCode(code);
	}



	@Override
	public Adm findEmail(String admEmail) throws Exception {
		Adm adma=admDao.findEmail(admEmail);
		return adma;
	}


	

}
