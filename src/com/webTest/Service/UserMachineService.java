package com.webTest.Service;

import java.util.List;

import com.webTest.Bean.Adm;
import com.webTest.Bean.PageBean;
import com.webTest.Bean.UserMachine;

public interface UserMachineService {

	public List<UserMachine> getProd(UserMachine userMachine);

	public void addM_code(UserMachine userMachine2);

	public boolean findM_code(UserMachine userMachine2);
	
	List<UserMachine> usermachinelist(UserMachine s_userMachine,PageBean pageBean);
	
	Long getusermachineCount(UserMachine s_userMachine);
	void upadateUserMachine(UserMachine s_userMachine);

	public Adm findAdm(Adm adm);

	public void add(UserMachine s_userMachine);

}
