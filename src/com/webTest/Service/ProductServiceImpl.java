package com.webTest.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webTest.Bean.PageBean;
import com.webTest.Bean.Product;
import com.webTest.Bean.Type;
import com.webTest.Dao.ProductDao;

@Service("productService")
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductDao productDao;
	

	public ProductDao getProductDao() {
		return productDao;
	}

	public void setProductDao(ProductDao productDao) {
		this.productDao = productDao;
	}

	@Override
	public List<Product> getProductList(Product s_product, PageBean pageBean) {
		List<Product> list=productDao.getProductList(s_product, pageBean);
		return list;
	}

	@Override
	public Long getProductCount(Product s_product) {
		Long long1=productDao.getProductCount(s_product);
		return long1;
	}

	@Override
	public void saveProduct(Product product) {
			productDao.saveProduct(product);
	}

	@Override
	public Product getID(int id) {
		Product product=productDao.getID(id);
		return product;
	}

	@Override
	public void deletProduct(Product product) {
			productDao.deletProduct(product);
	}

	@Override
	public List<Product> getProductTID(int tid) {
		List<Product> list=productDao.getProductTID(tid);
		return list;
	}

	@Override
	public List<Type> getTypeTID(int pid) {
		List<Type> list=productDao.getTypeTID(pid);
		return list;
	}

	@Override
	public List<Product> getAllProduct() {
		return productDao.getAllProduct();
	}

}
