package com.webTest.Service;

import java.util.List;

import com.webTest.Bean.PageBean;
import com.webTest.Bean.Product;
import com.webTest.Bean.Type;

public interface ProductService {
	List<Product> getProductList(Product s_product, PageBean pageBean);
	Long getProductCount(Product s_product);
	void saveProduct(Product product);
	Product getID(int id);
	void deletProduct(Product product);
	List<Product> getProductTID(int tid);
	List<Type> getTypeTID(int pid);
	List<Product> getAllProduct();
}
