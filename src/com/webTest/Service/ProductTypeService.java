package com.webTest.Service;

import java.util.List;

import com.webTest.Bean.PageBean;
import com.webTest.Bean.Type;

public interface ProductTypeService {
	List<Type> types(Type s_type,PageBean pageBean);
	Long gettypecount(Type s_type);
	List<Type> getFirst();
	List<com.webTest.Bean.Type> getMouseOver(int tid);
	public void updateType(Type type);
	public List<Type> parentsType();
	public void insertType(Type type);
	public void deleteType(Type type);
	public Type get(int tid);
	List<Type> getSecond();

}
