package com.webTest.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webTest.Bean.Adm;
import com.webTest.Bean.PageBean;
import com.webTest.Bean.UserMachine;
import com.webTest.Dao.UserMachineDao;

@Service("userMachineService")
public class UserMachineServiceImpl implements UserMachineService {

	@Autowired
	private UserMachineDao userMachineDao;
	
	public UserMachineDao getUserMachineDao() {
		return userMachineDao;
	}

	public void setUserMachineDao(UserMachineDao userMachineDao) {
		this.userMachineDao = userMachineDao;
	}

	public List<UserMachine> getProd(UserMachine userMachine) {
		return userMachineDao.getProd(userMachine);
	}

	@Override
	public void addM_code(UserMachine userMachine2) {
		userMachineDao.addM_code(userMachine2);
	}

	@Override
	public boolean findM_code(UserMachine userMachine2) {
		return userMachineDao.findM_code(userMachine2);
	}

	@Override
	public List<UserMachine> usermachinelist(UserMachine s_userMachine, PageBean pageBean) {
		List<UserMachine> list=userMachineDao.usermachinelist(s_userMachine, pageBean);
		return list;
	}

	@Override
	public Long getusermachineCount(UserMachine s_userMachine) {
		Long long1=userMachineDao.getusermachineCount(s_userMachine);
		return long1;
	}

	@Override
	public void upadateUserMachine(UserMachine s_userMachine) {
		userMachineDao.upadateUserMachine(s_userMachine);
	}

	@Override
	public Adm findAdm(Adm adm) {
		return userMachineDao.findAdm(adm);
	}

	@Override
	public void add(UserMachine s_userMachine) {
		userMachineDao.add(s_userMachine);
		
	}

}
