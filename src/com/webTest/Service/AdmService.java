package com.webTest.Service;

import java.sql.SQLException;
import java.util.List;


import com.webTest.Bean.Adm;
import com.webTest.Bean.PageBean;
import com.webTest.Bean.UserMachine;

public interface AdmService {
	 List<Adm> getAdmlist(Adm s_adm,PageBean pageBean);
	 Long getcount(Adm s_adm);
	 Adm get(String admEmail,String password);
	 Adm get(int aid);
	 void deletAdm(Adm adm);
	 void addAdm(Adm adm);
	 boolean Register(Adm adm) throws Exception;
	 Adm findAdmByCode(String code) throws Exception;
	 Adm findEmail(String admEmail) throws SQLException, Exception;

}
