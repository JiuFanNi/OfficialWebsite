package com.webTest.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webTest.Bean.PageBean;
import com.webTest.Bean.Type;
import com.webTest.Dao.ProductTypeDao;

@Service("producttypeService")
public class ProductTypeServiceImpl implements ProductTypeService {	
	@Autowired
	private ProductTypeDao producttypeDao;

	

	public ProductTypeDao getProducttypeDao() {
		return producttypeDao;
	}

	public void setProducttypeDao(ProductTypeDao producttypeDao) {
		this.producttypeDao = producttypeDao;
	}

	@Override
	public List<Type> types(Type s_type, PageBean pageBean) {
		List<Type> list=producttypeDao.types(s_type, pageBean);
		return list;
	}

	@Override
	public Long gettypecount(Type s_type) {
		Long long1=producttypeDao.gettypecount(s_type);
		return long1;
	}

	@Override
	public List<Type> getFirst() {
		return producttypeDao.getFirst();
	}

	@Override
	public List<Type> getMouseOver(int tid) {
		return producttypeDao.getMouseOver(tid);
	}

	@Override
	public void updateType(Type type) {
		producttypeDao.updateType(type);
		
	}

	@Override
	public List<Type> parentsType() {
		return producttypeDao.parentsType();
	}

	@Override
	public void insertType(Type type) {
		producttypeDao.insertType(type);
		
	}

	@Override
	public void deleteType(Type type) {
		producttypeDao.deleteType(type);
		
	}

	@Override
	public Type get(int tid) {
		Type t=producttypeDao.get(tid);
		return t;
	}

	@Override
	public List<Type> getSecond() {
		return producttypeDao.getSecond();
	}

	
}
