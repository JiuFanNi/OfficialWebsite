package com.webTest.Bean;

import java.util.HashSet;
import java.util.Set;

/**
 * Type entity. @author MyEclipse Persistence Tools
 */

public class Type implements java.io.Serializable {

	// Fields

	private Integer tid;
	private Type ParentType;
	private String tname;
	private Integer pid;
	private Set products = new HashSet(0);
	private Set children= new HashSet(0);
	private String flag ;  //1默认  0假删
	private Integer id;
	private String text;
	
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Integer getId() {
		return tid;
	}

	public void setId(Integer id) {
		this.id = tid;
	}

	public String getText() {
		return tname;
	}

	public void setText(String text) {
		this.text = tname;
	}

	public Integer getPid(){
		if (ParentType==null) {
			return 0;
		}
		return ParentType.getTid();
	}
	// Constructors

	/** default constructor */
	public Type() {
	}

	/** minimal constructor */
	public Type(Integer tid) {
		this.tid = tid;
	}

	/** full constructor */

	public Type(Integer tid, Type parentType, String tname, Integer pid, Set products, Set children, String flag,
			Integer id, String text) {
		super();
		this.tid = tid;
		ParentType = parentType;
		this.tname = tname;
		this.pid = pid;
		this.products = products;
		this.children = children;
		this.flag = flag;
		this.id = id;
		this.text = text;
	}

	// Property accessors

	public Integer getTid() {
		return this.tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public String getTname() {
		return this.tname;
	}

	public void setTname(String tname) {
		this.tname = tname;
	}

	

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Set getProducts() {
		return this.products;
	}

	public void setProducts(Set products) {
		this.products = products;
	}

	public Set getChildren() {
		return children;
	}

	public void setChildren(Set children) {
		this.children = children;
	}

	public Type getParentType() {
		return ParentType;
	}

	public void setParentType(Type parentType) {
		ParentType = parentType;
	}
	

}