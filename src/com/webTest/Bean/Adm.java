package com.webTest.Bean;

import java.util.HashSet;
import java.util.Set;

/**
 * Adm entity. @author MyEclipse Persistence Tools
 */

public class Adm implements java.io.Serializable {

	// Fields

	private Integer aid;
	private String admName;
	private String admPassword;
	private String admMoble;
	private String admEmail;
	private Integer state;
	private String code;
	private Set userMachines = new HashSet(0);
	private String flag = "1";  //1默认  0假删

	// Constructors

	/** default constructor */
	public Adm() {
	}

	/** full constructor */
	public Adm(Integer aid, String admName, String admPassword, String admMoble, String admEmail, Integer state,
			String code, Set userMachines, String flag) {
		super();
		this.aid = aid;
		this.admName = admName;
		this.admPassword = admPassword;
		this.admMoble = admMoble;
		this.admEmail = admEmail;
		this.state = state;
		this.code = code;
		this.userMachines = userMachines;
		this.flag = flag;
	}
	// Property accessors
	
	public Integer getAid() {
		return this.aid;
	}

	

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public void setAid(Integer aid) {
		this.aid = aid;
	}

	public String getAdmName() {
		return this.admName;
	}

	public void setAdmName(String admName) {
		this.admName = admName;
	}

	public String getAdmPassword() {
		return this.admPassword;
	}

	public void setAdmPassword(String admPassword) {
		this.admPassword = admPassword;
	}

	public String getAdmMoble() {
		return this.admMoble;
	}

	public void setAdmMoble(String admMoble) {
		this.admMoble = admMoble;
	}

	public String getAdmEmail() {
		return this.admEmail;
	}

	public void setAdmEmail(String admEmail) {
		this.admEmail = admEmail;
	}

	public Integer getState() {
		return this.state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set getUserMachines() {
		return this.userMachines;
	}

	public void setUserMachines(Set userMachines) {
		this.userMachines = userMachines;
	}

}