package com.webTest.Bean;

import java.util.HashSet;
import java.util.Set;

/**
 * Product entity. @author MyEclipse Persistence Tools
 */

public class Product implements java.io.Serializable {

	// Fields

	private Integer id;
	private Type type;
	private String name;
	private String description;
	private Double price;
	private String productpath;
	private String path;
	private Set userMachines = new HashSet(0);
	private String flag = "1";  //1默认  0假删

	// Constructors

	/** default constructor */
	public Product() {
	}

	/** full constructor */

	public Product(Integer id, Type type, String name, String description, Double price, String productpath,
			String path, Set userMachines, String flag) {
		super();
		this.id = id;
		this.type = type;
		this.name = name;
		this.description = description;
		this.price = price;
		this.productpath = productpath;
		this.path = path;
		this.userMachines = userMachines;
		this.flag = flag;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Type getType() {
		return this.type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return this.price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getProductpath() {
		return this.productpath;
	}

	public void setProductpath(String productpath) {
		this.productpath = productpath;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Set getUserMachines() {
		return this.userMachines;
	}

	public void setUserMachines(Set userMachines) {
		this.userMachines = userMachines;
	}

}