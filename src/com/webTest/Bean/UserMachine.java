package com.webTest.Bean;

/**
 * UserMachine entity. @author MyEclipse Persistence Tools
 */

public class UserMachine implements java.io.Serializable {

	// Fields

	private Integer id;
	private Adm adm;
	private Product product;
	private String MCode;

	// Constructors

	/** default constructor */
	public UserMachine() {
	}

	/** minimal constructor */
	public UserMachine(Adm adm) {
		this.adm = adm;
	}

	/** full constructor */
	public UserMachine(Adm adm, Product product, String MCode) {
		this.adm = adm;
		this.product = product;
		this.MCode = MCode;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Adm getAdm() {
		return this.adm;
	}

	public void setAdm(Adm adm) {
		this.adm = adm;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getMCode() {
		return this.MCode;
	}

	public void setMCode(String MCode) {
		this.MCode = MCode;
	}

	@Override
	public String toString() {
		return "UserMachine [id=" + id + ", adm=" + adm + ", product=" + product + ", MCode=" + MCode + "]";
	}
	
}