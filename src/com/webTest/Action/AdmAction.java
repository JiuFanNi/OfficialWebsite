package com.webTest.Action;



import java.io.IOException;



import java.util.List;



import javax.annotation.Resource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;






import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.springframework.stereotype.Controller;

import com.webTest.Bean.PageBean;
import com.webTest.Bean.UserMachine;
import com.webTest.tools.ResponseUtil;
import com.opensymphony.xwork2.ActionSupport;
import com.webTest.Bean.Adm;


import com.webTest.Service.AdmService;
import com.webTest.tools.Md5Test;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;


@Controller("/admAction")
public class AdmAction extends ActionSupport implements ServletRequestAware,ServletResponseAware{
	private static final long serialVersionUID = 1L;
	@Resource
	private AdmService admService; 
	private HttpServletRequest request;
	private HttpServletResponse response;
	private Adm adm;
	private String page;
	private String rows;
	public String code;
	private Adm s_adm;
	private String ids;
	private Md5Test md5Test;
	
	
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public Adm getS_adm() {
		return s_adm;
	}
	public void setS_adm(Adm s_adm) {
		this.s_adm = s_adm;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getRows() {
		return rows;
	}
	public void setRows(String rows) {
		this.rows = rows;
	}
	
	public AdmService getAdmService() {
		return admService;
	}
	public void setAdmService(AdmService admService) {
		this.admService = admService;
	}

	public Md5Test getMd5Test() {
		return md5Test;
	}
	public void setMd5Test(Md5Test md5Test) {
		this.md5Test = md5Test;
	}
	public Adm getAdm() {
		return adm;
	}
	public void setAdm(Adm adm) {
		this.adm = adm;
	}
	//用户登录
	public String login() throws ServletException, IOException{
		HttpSession session=request.getSession();
		String emailin=request.getParameter("admEmail");
		String passin = request.getParameter("admPassword");
		md5Test = new Md5Test();
		passin = md5Test.toMD5(passin);
		Adm ad=admService.get(emailin,passin);
		if(ad==null){
			request.setAttribute("msg", "您输入的信息有误");
			request.getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);
		}else if(ad.getState()==0) {
			request.setAttribute("msg", "请激活后登陆");
			request.getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);
		}else {
			session.setAttribute("admName",ad.getAdmName());
			request.setAttribute("admName", ad.getAdmName());
//			request.getRequestDispatcher("index.jsp").forward(request, response);
			response.sendRedirect(request.getContextPath()+"/index.jsp");
		}
		return null;
	
	}
	

//用户注册
	public String register() throws Exception,ServletException,IOException {
			try {
				Adm adm=new Adm();
				adm.setAdmName(request.getParameter("admName"));
				adm.setAdmEmail(request.getParameter("admEmail"));
				adm.setAdmMoble(request.getParameter("admMoble"));
				adm.setState(0);
				String code=Md5Test.getUUID()+Md5Test.getUUID();
				adm.setCode(code);
				String passin = request.getParameter("admPassword");
				md5Test = new Md5Test();
				passin = md5Test.toMD5(passin);
				adm.setAdmPassword(passin);
				Adm adm2=admService.findEmail(request.getParameter("admEmail"));
				if (null !=adm2 ) {
					if (adm2.getState()!= 0) {
						request.setAttribute("msg", "邮箱已经被注册");
						request.getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);
					}else {
						request.setAttribute("msg", "您已注册了，请前往邮箱激活");
						request.getRequestDispatcher("/WEB-INF/result.jsp").forward(request, response);
					}
				}else {
					if (admService.Register(adm)) {
						request.setAttribute("mesg", "您已经注册成功请前往邮箱激活！");
						request.getRequestDispatcher("/WEB-INF/return.jsp").forward(request, response);
					}else{
						return "register";
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return null;	
	}

	//查询用户表
	public String getAdmList() throws Exception{
		PageBean pageBean=new PageBean(Integer.parseInt(page), Integer.parseInt(rows));
		List<Adm> list =admService.getAdmlist(s_adm, pageBean);
		long total=admService.getcount(s_adm);
		JsonConfig jsonConfig=new JsonConfig();
		jsonConfig.setExcludes(new String[] {"userMachines"});
		JSONArray rows=JSONArray.fromObject(list, jsonConfig);
		JSONObject result=new JSONObject();
		result.put("rows", rows);
		result.put("total", total);
		ResponseUtil.write(ServletActionContext.getResponse(), result);
		return null;
	}
	public String addAdm() throws Exception{
		admService.addAdm(adm);
		JSONObject result=new JSONObject();
		result.put("success", true);
		ResponseUtil.write(ServletActionContext.getResponse(), result);
		return null;
	}
	public String deleteAdm() throws Exception{
		JSONObject result=new JSONObject();
		String []idsStr=ids.split(",");
		for(int i=0;i<idsStr.length;i++){
			Adm a=admService.get(Integer.parseInt(idsStr[i]));
			admService.deletAdm(a);
		}
		result.put("success", true);
		ResponseUtil.write(ServletActionContext.getResponse(), result);
		return null;
	}
	@Override
	public void setServletResponse(HttpServletResponse response) {
		// TODO Auto-generated method stub
		this.response=response;
	}
	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request=request;
	}

	
	

}
