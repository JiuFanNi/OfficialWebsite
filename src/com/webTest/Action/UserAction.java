package com.webTest.Action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.webTest.Bean.User;
import com.webTest.Service.UserService;
import com.webTest.tools.Md5Test;
@Controller("userAction")
public class UserAction extends ActionSupport{
	private int id;
	private String userName;
	private String userPassword;
	@Resource
	private UserService userService; 
	private List<User> users;
	@Resource
	private Md5Test md5Test;
    public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserList(){
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpServletResponse response = ServletActionContext.getResponse();
		String nameinfo = request.getParameter("userName");
		String passinfo = request.getParameter("userPassword");
		
		md5Test = new Md5Test();

		passinfo = md5Test.toMD5(passinfo);
		users = userService.getUserList();	
		if (nameinfo.equals(users.get(0).getUserName())&&passinfo.equals(users.get(0).getUserPassword())) {
			return "success";
		}else {
			return "login";
		}
	}
	
	
	public String returnadmin(){
		return "loginht";
	}
	
	

}
