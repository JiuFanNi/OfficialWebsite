package com.webTest.Action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.opensymphony.xwork2.ActionSupport;
import com.webTest.Bean.PageBean;
import com.webTest.Bean.Product;
import com.webTest.Bean.Type;
import com.webTest.Service.ProductTypeService;
import com.webTest.tools.ResponseUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

@Controller("/productTypeAction")
public class ProductTypeAction extends ActionSupport  {
	private static final long serialVersionUID = 1L;

	
	@Resource
	private ProductTypeService producttypeService;
	
	private String page;
	private String rows;
	private Type s_type;
	private String ids;
	
	public String getPage() {
		return page;
	}


	public void setPage(String page) {
		this.page = page;
	}


	public String getRows() {
		return rows;
	}


	public void setRows(String rows) {
		this.rows = rows;
	}




	public Type getS_type() {
		return s_type;
	}


	public void setS_type(Type s_type) {
		this.s_type = s_type;
	}


	public String getIds() {
		return ids;
	}


	public void setIds(String ids) {
		this.ids = ids;
	}


	public ProductTypeService getProducttypeService() {
		return producttypeService;
	}


	public void setProducttypeService(ProductTypeService producttypeService) {
		this.producttypeService = producttypeService;
	}


	public String gettype() throws Exception{
		PageBean pageBean=new PageBean(Integer.parseInt(page), Integer.parseInt(rows));
		List<Type> list =producttypeService.types(s_type, pageBean);
		
		long total=producttypeService.gettypecount(s_type);
		JsonConfig jsonConfig=new JsonConfig();
		jsonConfig.setIgnoreDefaultExcludes(false);
		jsonConfig.setExcludes(new String[] {"parentType","children","products"});
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);  
		JSONArray rows=JSONArray.fromObject(list, jsonConfig);
//		String rows=JSONArray.fromObject(list, jsonConfig).toString();
		JSONObject result=new JSONObject();
		result.put("rows", rows);
		result.put("total", total);
		ResponseUtil.write(ServletActionContext.getResponse(), result);
		return null;
	}
	
	
	//首页展示
	public String getFirst() throws Exception {
		HttpServletRequest req = ServletActionContext.getRequest();
		  //查询所有
		List<Type> list = producttypeService.getFirst();
		List<Type> listSec = producttypeService.getSecond();
		
		req.setAttribute("list", list);   //所有大类的展示
		req.setAttribute("listSec", listSec);   //所有大类的展示
		return "index2";
	}
	
	private List<Type> typeList;   //存放 鼠标聚焦的子类型
	private String rightPage;
	public List<Type> getTypeList() {
		return typeList;
	}
	public void setTypeList(List<Type> typeList) {
		this.typeList = typeList;
	}
	public String getRightPage() {
		return rightPage;
	}
	public void setRightPage(String rightPage) {
		this.rightPage = rightPage;
	}


	//鼠标聚焦时，取数据
	public String getMouseOver(){
		HttpServletRequest req = ServletActionContext.getRequest();
				//根据tid查找符合条件的子类型
		int tid = Integer.parseInt(req.getParameter("tid"));
		typeList = producttypeService.getMouseOver(tid); // 教育  工业 ---> 下一级
		rightPage = "right.jsp";
		req.setAttribute("typeList", typeList);   //所有大类的展示
		return null;
	}
	
	
	public String getMouseOverTwo(){
		HttpServletRequest req = ServletActionContext.getRequest();
		//根据tid查找符合条件的子类型
		int tid = Integer.parseInt(req.getParameter("tid"));
		List<Type> list = producttypeService.getMouseOver(tid);
		Type t = (Type)list.get(0);
		 for (Type type : list) {
			System.out.println(type.getTname()+"---------");
		}
				//查找父类型
		 List<Type> parentList = producttypeService.getFirst();
			Type type0 = (Type)parentList.get(0);   //产品购买
		//大类的名字  ：产品购买  服务 
			for (int i = 0; i < parentList.size(); i++) {
				Type s = (Type) parentList.get(i);
				req.setAttribute("typeName"+(i+1), s.getTname());
			}
		 Set<Type> set0 = type0.getChildren();  //教育软件   工业培训软件
		req.setAttribute("type", list);
		req.setAttribute("list1", set0);
		req.setAttribute("list", parentList);   //所有大类的展示
		return "index2";
	}
	
	public String updateType() throws Exception {
		s_type.setPid(s_type.getParentType().getTid());
		producttypeService.updateType(s_type);
		JSONObject result=new JSONObject();
		result.put("success", true);
		ResponseUtil.write(ServletActionContext.getResponse(), result);
		return null;
	}
	
	public String parentsType() throws Exception{
		List<Type> list=producttypeService.parentsType();
		JsonConfig jsonConfig=new JsonConfig();
		jsonConfig.setIgnoreDefaultExcludes(false);
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);  
		jsonConfig.setExcludes(new String[] {"products","pid","parentType"});
		String rows=JSONArray.fromObject(list, jsonConfig).toString();
		ServletActionContext.getResponse().setContentType("text/json;charset=utf-8");
		ServletActionContext.getResponse().getWriter().print(rows);
		return null;
	}
	public String insertType() throws Exception {
		if (s_type.getParentType().getTid()==null) {
			s_type.setParentType(null);
			s_type.setPid(0);
		}else{
			s_type.setPid(s_type.getParentType().getTid());
		}

		s_type.setFlag("1");
		producttypeService.insertType(s_type);
		JSONObject result=new JSONObject();
		result.put("success", true);
		ResponseUtil.write(ServletActionContext.getResponse(), result);
		return null;
	}
	public String deleteType() throws Exception {
		Type type=new Type();
		JSONObject result=new JSONObject();
		String []idsStr=ids.split(",");
		for(int i=0;i<idsStr.length;i++){
			type=producttypeService.get(Integer.parseInt(idsStr[i]));
			producttypeService.deleteType(type);
		}
		result.put("success", true);
		ResponseUtil.write(ServletActionContext.getResponse(), result);
		return null;
	}
	
	
}
